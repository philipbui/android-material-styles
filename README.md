# Android Material Styles

[![Build Status](https://travis-ci.org/philip-bui/android-material-styles.svg?branch=master)](https://travis-ci.org/philip-bui/android-material-styles)
[![Release](https://jitpack.io/v/philip-bui/android-material-styles.svg)](https://jitpack.io/#philip-bui/android-material-styles)


I've been wanting to write this down, due to the vast specs of Material Design and my limited memory span (or unwillingness to memorize), I have written this guide that concerns only the IMPLEMENTATION of Material Design.

NOTE: This actual Library Project is for Personal Use, contains Material Styled Items. You are free to use it, but this is my own implementation for my reuse.

## Style

### Icons

For the dimens, I'd recommend creating a style instead of putting it in the dimens.xml. 

- Instead of doing ```"android:layout_width="@dimen/product_icon"``` much faster to use ```"style="@style/ProductIcon"```. 

- And instead of putting it into dimens and styles, just put the actual dimens into styles and forget (Just remember, there is 0% chance you will refactor it).

For the tinting, I'd recommend using a selector drawable with a black (or whatever color) image and set the alpha. Why? I'm assuming if you care about enabled, disabled color there's the offchance you'll come back to do a pressed, highlighted state later on.

### Typography

Fortunately, this is covered for you in the AppCompat themes.

Just use textAppearance="@style/TextAppearance.AppCompat.YourChosenTextHere".

## Components

### Bottom Navigation

I'd highly recommend you use either [roughike's BottomBar](https://github.com/roughike/BottomBar) or [aurelhubert's ahbottomnavigation](https://github.com/aurelhubert/ahbottomnavigation).

Coming from an iOS background, you may have 2 images for selected (filled) and unselected (unfilled). You will have to modify the library yourselves (About 3 lines in the right place).

### Buttons

- For Material Button Raised, just use ```"@style/Widget.AppCompat.Button.Colored"```

- For Material Button Flat, just use ```"@style/Widget.AppCompat.Button.Borderless"```

- For Material Button Flat (with colored text), just use ```@style/Widget.AppCompat.Button.Borderless.Colored"```

To style the actual color, just extend the style and use ```colorButtonNormal``` (Background) and ```android:textColor```.

### Buttons (Floating Action Button)

Fortunately, the ```'com.android.support:design:23.1.1'``` library has the base Floating Action Button.

To use Floating Action Menus (FAB that pops up more FAB to choose from), I'd recommend [Clans FloatingActionButton library](https://github.com/Clans/FloatingActionButton).

To hide on scroll, just set a OnScrollListener and do fab.hide() or fab.show();

### Cards

Part of the ```'com.android.support:cardview-v7:21.0.+'``` library.

### Chips

I'd recommend [splitwise's TokenAutoComplete](https://github.com/splitwise/TokenAutoComplete) as it is very extensible and can be used with any models, unlike the other libraries.

### Dialogs

To customize the default Android dialogs into Material, just extend or use ```Theme.AppCompat.Light.Dialog.Alert```.

- Button Color ```"colorAccent"```

- Text Color ```"android:textColorPrimary"```

- Background Color ```"android:background"```

To get more functionality (E.g a Title, Message and List together) I'd recommend [afollestad's material-dialogs library](https://github.com/afollestad/material-dialogs).



### Divider

Light-themed: ```#1E000000```

### Lists

Unfortunately, there is no existing implementation so you will have to hard-code these. I'd recommend putting most dimens inside the actual styles and only the reusable dimens into dimens.xml.

### Snackbar and Toasts

Snackbar is included in the ```'com.android.support:design:23.0.1'``` library (Only one action however).

Just use Snackbar.make(View, CharSequence/Res, Duration(Indenfinite, Long, Short Int constants)).

To style the text, use ```((TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text)).setTextColor(Your_color);```

To style the window, use ```snackbar.getView().setBackgroundColor(Your_color);```

### Subheaders

Largely up to you to implement, you can use ```android:textAppearance="TextAppearance.AppCompat.Subhead"``` and extend your Base style for Primary and Title colors

### TextFields

Part of the ```'com.android.support:design:23.1.1'``` library.

Just replace TextInputLayout with a nested TextInputEdit instead of EditText. 

To style the TextInputLayout, use:

- Hint and Label Unactivated Color ```"android:textColorHint"```

- Label Color Activated ```"colorAccent"```

- Bar Color Unactivated ```"colorControlNormal"```

- Bar Color Activated ```"colorControlActivated"```

For errors, remember to set errors on the TextInputLayout 

- You can use view.getParent() if you'd rather not keep a reference, but you can also use ```textInputLayout.findViewById(your TextInputEdit id)```

For counters, use ```app:counterEnabled``` and ```app:counterMaxLength```. To style, use ```counterOverflowTextAppearance ``` and ```counterTextAppearance ```.

## Tips

- Feel free to use ```android:theme``` to apply certain coloring.

- For pressedStates for drawables, use a layer-list with an opacity-based color for the circle.

- If you're using fully colored drawables, for the disabled state just set alpha to 0.54.

- Remember, Material Colors are a guideline. You can still use your corporate colors
