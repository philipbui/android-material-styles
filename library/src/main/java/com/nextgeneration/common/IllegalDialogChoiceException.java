package com.nextgeneration.common;

import android.content.DialogInterface;

@SuppressWarnings("unused")
public class IllegalDialogChoiceException extends IllegalArgumentException {

    public IllegalDialogChoiceException(DialogInterface dialogInterface, int choice) {
        super("Illegal menu choice " + choice + " for " + dialogInterface);
    }
}
