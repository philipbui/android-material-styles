package com.nextgeneration.common;


import android.view.View;

@SuppressWarnings("unused")
public class IllegalViewException extends IllegalArgumentException {

    public IllegalViewException(View view) {
        super("Unhandled View Click for " + view);
    }
}
