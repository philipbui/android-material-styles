package com.nextgeneration.service;

import android.app.Activity;

import com.nextgeneration.app.LoadingDialog;

@SuppressWarnings("unused")
public class LoadingDialogManager {

    private final Activity activity;
    private LoadingDialog loadingDialog;

    public LoadingDialogManager(Activity activity) {
        this.activity = activity;
    }

    public void show() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(activity);
        }
        loadingDialog.show();
    }

    public void dismiss() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    public void hide() {
        if (loadingDialog != null) {
            loadingDialog.hide();
        }
    }
}
