package com.nextgeneration.app;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

@SuppressWarnings("unused")
public class LoadingDialogTransparent extends LoadingDialog {

    public LoadingDialogTransparent(Context context) {
        super(context);
        init(context);
    }

    public LoadingDialogTransparent(Context context, int theme) {
        super(context, theme);
        init(context);
    }

    private void init(Context context) {
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }
}
