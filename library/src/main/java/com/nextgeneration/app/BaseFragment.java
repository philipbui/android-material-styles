package com.nextgeneration.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.nextgeneration.materialstyle.R;

/**
 * Base Fragment that overrides Transitions
 */
@SuppressWarnings("unused")
public abstract class BaseFragment extends Fragment {

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        if (getActivity() != null) {
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        }
    }

    @Override
    public void startActivity(Intent intent, @Nullable Bundle options) {
        super.startActivity(intent, options);
        if (getActivity() != null) {
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        }
    }
}
