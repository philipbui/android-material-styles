package com.nextgeneration.app;

import android.app.ProgressDialog;
import android.content.Context;

import com.nextgeneration.materialstyle.R;

@SuppressWarnings("unused")
public class LoadingDialog extends ProgressDialog {

    public LoadingDialog(Context context) {
        super(context);
        init(context);
    }

    @SuppressWarnings("WeakerAccess")
    public LoadingDialog(Context context, int theme) {
        super(context, theme);
        init(context);
    }

    private void init(Context context) {
        this.setIndeterminate(true);
        this.setMessage(context.getString(R.string.material_loading_dialog));
        this.setCancelable(false);
    }
}
